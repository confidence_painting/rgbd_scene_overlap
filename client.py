import zmq
import json
import time
import os

REQUEST_TIMEOUT = 2500
REQUEST_RETRIES = 300
SERVER_ENDPOINT = "tcp://localhost:5556"

context = zmq.Context()

client = context.socket(zmq.REQ)
client.setsockopt( zmq.RCVTIMEO, 5000 )
client.connect(SERVER_ENDPOINT)

poll = zmq.Poller()
poll.register(client, zmq.POLLIN)

base_path = "/externd/datasets/gibson_captured_256/gibson"

# Boxes are aligned by index
request_data = {
    "depth_path1": os.path.join(base_path, "Adrian/depth/00040.tiff"),
    "pose_path1": os.path.join(base_path, "Adrian/metadata/00040.json"),
    "depth_path2": os.path.join(base_path, "Adrian/depth/00061.tiff"),
    "pose_path2": os.path.join(base_path, "Adrian/metadata/00061.json"),
    "perturbation": 1,
    "calib1": {"fx":128, "fy":128, "cx":128, "cy":128},
    "calib2": {"fx":128, "fy":128, "cx":128, "cy":128},
    "boxes1": [
        {"x": 0, "y": 0.25, "w": 0.5, "h": 0.5},
        {"x": 0, "y": 0, "w": 1, "h": 0.2},
        {"x": 0, "y": 0, "w": 0.5, "h": 1},
    ],
    "boxes2": [
        {"x": 0.5, "y": 0, "w": 0.5, "h": 1},
        {"x": 0, "y": 0.8, "w": 1, "h": 0.2},
        {"x": 0.3, "y": 0.3, "w": 0.6, "h": 0.7},
    ]
} 

request_data["boxes1"] = request_data["boxes1"] * 1000
request_data["boxes2"] = request_data["boxes2"] * 1000

retries_left = REQUEST_RETRIES
while retries_left:
    request = json.dumps(request_data).encode()
    # print("I: Sending (%s)" % request)
    client.send(request)

    expect_reply = True
    while expect_reply:
        socks = dict(poll.poll(REQUEST_TIMEOUT))
        if socks.get(client) == zmq.POLLIN:
            reply = client.recv()
            if not reply:
                break
            # Check reply
            try:
                message = json.loads(reply)
                print("I: Server replied OK (%s)" % reply)
                retries_left = REQUEST_RETRIES
                expect_reply = False
            except:
                print("E: Malformed reply from server: %s" % reply)

        else:
            print("W: No response from server, retrying…")
            # Socket is confused. Close and remove it.
            client.setsockopt(zmq.LINGER, 0)
            client.close()
            poll.unregister(client)
            retries_left -= 1
            if retries_left == 0:
                print("E: Server seems to be offline, abandoning")
                break
            print("I: Reconnecting and resending")
            # Create new connection
            client = context.socket(zmq.REQ)
            client.connect(SERVER_ENDPOINT)
            poll.register(client, zmq.POLLIN)
            client.send(request)

context.term()
