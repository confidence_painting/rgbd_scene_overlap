import cv2
import open3d
import json
import numpy as np
from pyquaternion import Quaternion

def load_transform(path):
    with open(path, "r") as f:
        dat = json.load(f)
        q = Quaternion(x=dat["orientation"][0], y=dat["orientation"][1], z=dat["orientation"][2], w=dat["orientation"][3])
        corr = Quaternion(axis=np.array([1, 0, 0]), degrees=-90) * Quaternion(axis=np.array([0, 1, 0]), degrees=90)
        trans = (q*corr).transformation_matrix
        trans[0, 3] = dat["position"][0]
        trans[1, 3] = dat["position"][1]
        trans[2, 3] = dat["position"][2]
        return trans

col1 = open3d.Image(cv2.imread("/externd/datasets/views/gibson/Adrian/rgb/0000360.png", -1))
col2 = open3d.Image(cv2.imread("/externd/datasets/views/gibson/Adrian/rgb/0000496.png", -1))

dep1 = open3d.Image(cv2.imread("/externd/datasets/views/gibson/Adrian/depth/0000360.tiff", -1))
dep2 = open3d.Image(cv2.imread("/externd/datasets/views/gibson/Adrian/depth/0000496.tiff", -1))

trans1 = load_transform("/externd/datasets/views/gibson/Adrian/metadata/0000360.json")
trans2 = load_transform("/externd/datasets/views/gibson/Adrian/metadata/0000496.json")

intrinsic = open3d.camera.PinholeCameraIntrinsic(width=512, height=512, fx=256, fy=256, cx=256, cy=256)
rgbd1 = open3d.geometry.create_rgbd_image_from_color_and_depth(col1, dep1, depth_scale=512.0)
rgbd2 = open3d.geometry.create_rgbd_image_from_color_and_depth(col2, dep2, depth_scale=512.0)
pc1 = open3d.geometry.create_point_cloud_from_rgbd_image(rgbd1, intrinsic, extrinsic=trans1)
pc2 = open3d.geometry.create_point_cloud_from_rgbd_image(rgbd2, intrinsic, extrinsic=trans2)
open3d.draw_geometries([pc1, pc2])
