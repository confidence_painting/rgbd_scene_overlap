import cv2, sys
import numpy as np

if len(sys.argv) != 4:
    print("Usage: python view.py <path to index> <path to image list> <path to images>")

path_to_index = sys.argv[1]
path_to_imlist = sys.argv[2]
path_to_images = sys.argv[3]

imlist = []
with open(path_to_imlist, "r") as f:
    for line in f:
        imlist.append(line.strip())

print(len(imlist))

gt = cv2.imread(path_to_index, -1)

pos = np.where(gt == 1)
neg = np.where(gt == 0)
print(pos)

cv2.namedWindow("Pos")
cv2.namedWindow("Neg")

for i in range(100):
    p1, p2 =  imlist[pos[0][i]], imlist[pos[1][i]]
    n1, n2 = imlist[neg[0][i]], imlist[neg[1][i]]
    print(p1, p2)
    print(n1, n2)
    im_p1 = cv2.imread(path_to_images + p1, -1)
    im_p2 = cv2.imread(path_to_images + p2, -1)
    im_n1 = cv2.imread(path_to_images + n1, -1)
    im_n2 = cv2.imread(path_to_images + n2, -1)
    cv2.imshow("Pos", np.vstack((im_p1, im_p2)))
    cv2.imshow("Neg", np.vstack((im_n1, im_n2)))
    cv2.waitKey(0)
