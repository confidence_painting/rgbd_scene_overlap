import h5py
import numpy as np

f = h5py.File("../raw/stanford/area_3/3d/pointcloud.mat")

name_refs = f['Area_3']['Disjoint_Space']['name']

def get_obj(row):
  return f[h5py.h5r.get_name(row[0], f.id)]

names = map(get_obj, name_refs)
print(names)

object_refs = f['Area_3']['Disjoint_Space']['object']
objects = []

for i in object_refs:
  name = h5py.h5r.get_name(i[0], f.id)
  objects.append(f[name])
print(objects)
h5py.h5r.get_name(f['Area_3']['Disjoint_Space']['object'][1][0], f.id)
