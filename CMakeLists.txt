cmake_minimum_required(VERSION 2.8.11)
project(rgbd_dataset)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

find_package(OpenCV 3 REQUIRED)
include_directories(${OPENCV_INCLUDE_DIRS})

find_package(Eigen3 REQUIRED NO_MODULE)
include_directories(${EIGEN3_INCLUDE_DIR})
add_definitions( ${EIGEN3_DEFINITIONS} )

#find_package(PCL 1 REQUIRED)
#include_directories(${PCL_INCLUDE_DIRS})
#add_definitions(${PCL_DEFINITIONS})
#link_directories(${PCL_LIBRARY_DIRS})

#find_package(VTK REQUIRED)
#include(${VTK_USE_FILE})

# find_package(fmt)

FIND_PACKAGE( Boost COMPONENTS system filesystem program_options REQUIRED )
include_directories( ${Boost_INCLUDE_DIR} )

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-omit-frame-pointer")# -fsanitize=address")
set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-omit-frame-pointer")# -fsanitize=address")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${Open3D_EXE_LINKER_FLAGS} -fno-omit-frame-pointer")

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

find_package(Open3D HINTS ${CMAKE_INSTALL_PREFIX}/lib/CMake)
list(APPEND Open3D_LIBRARIES dl)
add_compile_options(-Wno-deprecated-declarations)
add_compile_options(-Wno-unused-result)

add_definitions(
	-Wall
  -std=c++11
  #-fext-numeric-literals
  #-fsanitize=address
  #-std=c++0x
  #-lasan
  -pg

  -D_FORTIFY_SOURCE=2
  -O2
  -g
  -rdynamic
  #-frename-registers
)

set(ALL_LIBS
	${OpenCV_LIBS}
	#${PCL_LIBRARIES}
  ${Boost_LIBRARIES} 
  ${VTK_LIBRARIES}
  ${Open3D_LIBRARIES}
  ${PNG_LIBRARY}
  -lcurl
  # -lasan
  -lzmq
  ${EIGEN_DIR}
)

set(CORE_CPP
  "src/util.cpp"
  "src/Stanford.cpp"
  "src/MIT.cpp"
  "src/Gibson.cpp"
  "src/Scannet.cpp"
  "src/Processor.cpp"
  "src/Loader.cpp"
  "src/Sampler.cpp"
  "src/overlap.cpp"
)

#add_executable(rgbd_dataset src/main.cpp ${CORE_CPP})
#target_link_libraries(rgbd_dataset ${ALL_LIBS})

# add_executable(proc_gibsongen src/proc_gibsongen.cpp ${CORE_CPP})
# target_link_libraries(proc_gibsongen ${ALL_LIBS})
#
# add_executable(proc_stanford src/proc_stanford.cpp ${CORE_CPP})
# target_link_libraries(proc_stanford ${ALL_LIBS})
#
add_executable(proc_scannet src/proc_scannet.cpp ${CORE_CPP})
target_link_libraries(proc_scannet ${ALL_LIBS})

# add_executable(proc_mit src/proc_mit.cpp ${CORE_CPP})
# target_link_libraries(proc_mit ${ALL_LIBS})

add_executable(proc_gibson src/proc_gibson.cpp ${CORE_CPP})
target_link_libraries(proc_gibson ${ALL_LIBS})

# add_executable(overlap_server src/overlap_server.cpp ${CORE_CPP})
# target_link_libraries(overlap_server ${ALL_LIBS})

#add_library(overlap_lib SHARED src/overlap_lib.cpp)
#target_link_libraries(overlap_lib ${ALL_LIBS})
