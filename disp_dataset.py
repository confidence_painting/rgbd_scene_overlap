import cv2
import numpy as np
import sys
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("gt", type=str, help="path to ground truth matrix")
parser.add_argument("image_paths", type=str, help="path to list of images")
parser.add_argument("base_dir", type=str, help="path to all datasets and indexes")
args = parser.parse_args()

gt = cv2.imread(args.gt, -1)

paths = []
with open(args.image_paths, "r") as f:
    for line in f.readlines():
        paths.append(line.strip())

print(gt[:10, :10])
pos = np.where(gt == 1)
neg = np.where(gt == 0)

print("Displaying positives")
for i in range(len(pos[0])):
    p1_i, p2_i = pos[0][i], pos[1][i]
    p1_p, p2_p = os.path.join(args.base_dir, paths[p1_i]), os.path.join(args.base_dir, paths[p2_i])
    print(p1_p, p2_p)
    p1, p2 = cv2.imread(p1_p, -1), cv2.imread(p2_p, -1)
    cv2.imshow("POS", np.hstack((p1, p2)))
    cv2.waitKey(0)
