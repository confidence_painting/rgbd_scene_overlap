# RGBD Scene Overlap
This contains code for computing pairwise overlap between images that contain
depth and pose. The output format is an '.exr' image. The supported datasets are
the Gibson Random Views dataset (not yet released) and 
[Scannet](https://github.com/ScanNet/ScanNet). There are a few other datasets
with loaders, but these have not been tested for a long time after many changes
and will likely require modification.

## Installation
You need the following libraries: OpenCV and Open3D. OpenCV can be installed
from your OS package manager. On Ubuntu, this package is `libopencv-dev`. You
can install Open3D from the `pip` repository `open3d`. However, if this doesn't
work, we recommend that you build it from source or install it from the `AUR` if
you are using Arch Linux. Building the project will then generate executables in
the build directory that can be used to process the dataset.

## Processing
The Gibson Random Views processor is called `proc_gibson` while the Scannet
processor is called `proc_scannet`. The argument structure for every processor
is:
```
proc_<dataset> <path to dataset> <dataset subset number>
```
The path to the dataset for Scannet should be the path to the directory that
contains the `images` directory, while the path to the Gibson Random Views
dataset should contain the `gibson` directory. Within this path, you need to
create the directory `indexes/train`, otherwise nothing will be generated. Yes,
I know it's misspelled, but I'm in too deep.
The command I like to run to spin off the processing is
```
for i in $(seq 2 151); do ./proc_scannet ../../scannet/ $i --skipClouds 60 &; done  
```
Although I would like to say that this is probably very suboptimal in terms of
speed.

## Adding a new dataset
Implement the Dataset interface (look at the Stanford implementation).   
  (Don't forget to add the src file to the SRC file in CMakeLists)  
Write main class like `proc_stanford` and add it to the CMakeLists.  
Uncomment the lines in the Loader class to view the pointcloud to verify that your transforms are correct.  
Compile and run.
