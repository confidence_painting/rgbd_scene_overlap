#pragma once

#include <Eigen/Geometry>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "json.hpp"
#include "util.h"
#include "Dataset.h"
#include <assert.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
using namespace std;

static const string STANFORD_DATASETS[7] = {
  "area_1",
  "area_2",
  "area_3",
  "area_4",
  "area_5a",
  "area_5b",
  "area_6",
};

class Stanford: public Dataset {

  fs::path dataPath;
  vector<string> locationNames;
  int subset;

  vector<string> makeList(const fs::path & modalityPath);
  vector<string> makeListOfRooms(const fs::path & modalityPath);
  vector<string> makeRoomList(const fs::path & modalityPath, const string & room);
  string getRoomName( const fs::path & base_path, const int dataset_num, const int room_num);
  static Eigen::Matrix4f loadGroundTruth(const fs::path & poseFilePath);
  public:
    Stanford(const fs::path & basePath, int subset);
    int subsets;
    static float depthFactor;
    string path;
    vector<Eigen::Matrix4f> loadGroundTruths();
    vector<fs::path> loadImagePaths();
    vector<fs::path> loadCleanImagePaths();
    vector<fs::path> loadDepthPaths();
    vector<Calibration> loadCalibrations();
    string getName();
    float getDepthFactor();
};

