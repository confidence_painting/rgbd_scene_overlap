#pragma once

#include <iostream>
#include <eigen3/Eigen/Geometry>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Open3D/Open3D.h>
#include "util.h"

namespace o3 = open3d::geometry;

float
back_project(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const o3::PointCloud & cand_pc,
    const Eigen::Matrix4f & query_pose,
    const Eigen::Matrix4f & cand_pose,
    const Calibration & query_calib,
    const Calibration & cand_calib,
    const BoundingBox & query_bbx,
    const BoundingBox & cand_bbx,
    const float depthFactor,
    cv::Mat & backproj);

float
back_project(
    const cv::Mat & depth1,
    const cv::Mat & depth2,
    const Eigen::Matrix4f & pose1,
    const Eigen::Matrix4f & pose2,
    const Calibration & calib1,
    const Calibration & calib2,
    const BoundingBox & bb1,
    const BoundingBox & bb2,
    const float depthFactor,
    cv::Mat & backproj);

float
bbx_overlap(
    const cv::Mat & backprojected,
    const BoundingBox & bb);

open3d::geometry::PointCloud 
depthToCloud(
    const cv::Mat & depth,
    const cv::Mat * color,
    const Calibration calib,
    const float factor,
    const BoundingBox & bb,
    const bool interp_cloud,
    const int decimation_factor);

void
colorCloudToMat(
    cv::Mat & dst,
    const open3d::geometry::PointCloud & cloud,
    const Calibration & calib,
    int width, int height, float factor);
