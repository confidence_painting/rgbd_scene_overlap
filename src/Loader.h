#pragma once
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>
#include <Open3D/Open3D.h>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "util.h"
#include "json.hpp"
#include <assert.h>
#include "Dataset.h"
#include "overlap.h"

using namespace std;
namespace o3 = open3d::geometry;

class Loader {
  public:
    Dataset * ds;
    int maxMissingPoints, decimation_factor;
    float minEntropy;
    vector<fs::path> imagePaths, depthPaths;
    vector<Eigen::Matrix4f> transforms;
    vector<Calibration> calibs;

    Loader(
        Dataset * ds,
        int maxMissingPoints,
        int decimation_factor, 
        float minEntropy);

    int
    loadCloud(const int index, o3::PointCloud * pc);

    o3::PointCloud loadClouds(
        const int maxClouds,
        const int skipClouds, 
        vector<o3::PointCloud> * clouds);
};
