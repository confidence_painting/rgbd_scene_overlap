#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "mat4f.h"
#include "util.h"
#include "MIT.h"
#include "json.hpp"
#include <assert.h>
#include "Dataset.h"

using json = nlohmann::json;
namespace fs = boost::filesystem;

using namespace std;

MIT::MIT(const fs::path & basePath, int subset) {
  // List directory of scannet items
  this->subset = subset;
  datasetNames = makeList(basePath);
  dataPath = basePath / datasetNames[subset];
  printf("Loading images from %s\n", dataPath.string().c_str());
}

string MIT::getName() {
  return datasetNames[subset];
}

float
MIT::getDepthFactor() {
  return 575.8146337531022;
}

float MIT::depthFactor = 575.8146337531022;

Eigen::Matrix4f
MIT::loadGroundTruth(const fs::path & poseFilePath) {
  std::ifstream t(poseFilePath.string());
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());
  json js = json::parse(str);
  float s = 1;
  // Can't directly cast. C++ Magic
  float x = (js["position"][0]);
  float y = (js["position"][1]);
  float z = (js["position"][2]);
  Eigen::Translation3f trans (x*s, y*s, z*s);
  //Eigen::Translation3f trans (0,0,0);
  Eigen::Quaternionf q(js["orientation"][3],
                       js["orientation"][0],
                       js["orientation"][1],
                       js["orientation"][2]);
  //Eigen::Quaternionf q(1,
                       //0,
                       //0,
                       //0);
  Eigen::Quaternionf coc; // Translate to ROS Coords
  //coc = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());
  //coc = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX());
  coc = 
      Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    * Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitY())
    * Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());

  return (coc*trans*q*coc.inverse()).matrix();
}

vector<Eigen::Matrix4f> MIT::loadGroundTruths() {
  vector<string> l = makeList(dataPath / "poses");
  vector<Eigen::Matrix4f> transforms;
  transforms.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath / "poses" / fs::path(l.at(i));
    transforms.push_back(MIT::loadGroundTruth(depthPath));
  }
  return transforms;
}

vector<fs::path> MIT::loadCleanImagePaths() {
  vector<string> l = makeList(dataPath / "rgb");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = fs::path(getName()) / "rgb" / l.at(i);
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> MIT::loadImagePaths() {
  vector<string> l = makeList(dataPath / "rgb");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath /  "rgb" /fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> MIT::loadDepthPaths() {
  vector<string> l = makeList(dataPath / "depth");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath /  "depth" /fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<Calibration> MIT::loadCalibrations() {

  // Kinect fx fy cx cy
  Calibration c (572.882768, 542.739980, 314.649173, 240.160459);
  vector<string> _l = makeList(dataPath / "depth"); // Load to get size

  vector<Calibration> cs;
  cs.reserve(_l.size());
  for (size_t i=0; i < _l.size(); i++) {
    cs.push_back(c);
  }
  return cs;
}
