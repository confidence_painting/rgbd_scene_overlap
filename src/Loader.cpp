#include <eigen3/Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "util.h"
#include "json.hpp"
#include "Dataset.h"
#include "Loader.h"
#include <assert.h>

using namespace std;

Loader::Loader(
    Dataset * ds,
    int maxMissingPoints,
    int decimation_factor, 
    float minEntropy)
  : ds(ds),
    maxMissingPoints(maxMissingPoints),
    decimation_factor(decimation_factor),
    minEntropy(minEntropy)
{
  this->imagePaths = ds->loadImagePaths();
  this->depthPaths = ds->loadDepthPaths();
  if (this->imagePaths.size() > 0) {
    cout << "Sample color path: " << this->imagePaths[0] << endl;
    cout << "Sample depth path: " << this->depthPaths[0] << endl;
  }
  cout << "Loading calibrations" << endl;
  this->calibs = ds->loadCalibrations();
  cout << "Loading ground truths" << endl;
  this->transforms = ds->loadGroundTruths();
  //for (size_t i=0; i < this->imagePaths.size(); i++) {
    //cout << this->imagePaths[i] << endl;
  //}
}

int
Loader::loadCloud(const int index, o3::PointCloud * pc)
{
    // Load preTransform
    cv::Mat color = cv::imread(imagePaths[index].string(), -1);
    cv::Mat depth = cv::imread(depthPaths[index].string(), -1);

    // Compute image entropy
    /*
       This is overly aggressive. Go fix the dataset instead
    cv::Mat frame;
#if CV_MAJOR_VERSION > 3
    cvtColor(color,frame,cv::COLOR_BGR2GRAY);
#else
    cvtColor(color,frame,CV_BGR2GRAY);
#endif
    int histSize = 256;
    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    bool uniform = true; bool accumulate = false;
    /// Compute the histograms:
    cv::MatND hist;
    cv::calcHist( &frame, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
    hist /= frame.total();
    hist += 1e-4; //prevent 0

    cv::Mat logP;
    cv::log(hist,logP);

    float entropy = -1*cv::sum(hist.mul(logP)).val[0];
    if (entropy < minEntropy) {
      std::cout << "Rejected: " << imagePaths[index].string() << std::endl;
      return false;
    }
    */

    //Cloud * preTransform = new Cloud(depth.cols/decimation_factor, depth.rows/decimation_factor);
    // Resize image to the smallest one
    if (depth.cols > color.cols) {
      cv::resize(depth, depth, cv::Size2i(color.rows, color.cols));
      //cv::resize(color, color, cv::Size2i(color.rows/decimation_factor, color.cols/decimation_factor));
    } else if (depth.cols < color.cols) {
      cv::resize(color, color, cv::Size2i(depth.rows, depth.cols));
      //cv::resize(depth, depth, cv::Size2i(depth.rows/decimation_factor, depth.cols/decimation_factor));
    }
    //Cloud::Ptr preTransform (new Cloud(depth.cols, depth.rows));
    // Remove clouds with few points
    BoundingBox full(0, 0, depth.cols, depth.rows);
    *pc = depthToCloud(
        depth,
        NULL,
        calibs[index],
        ds->getDepthFactor(),
        full,
        true,
        decimation_factor);
    pc->Transform(transforms[index].cast<double>());
    return (depth.cols-decimation_factor)*(depth.rows-decimation_factor) - pc->points_.size();
}

o3::PointCloud
Loader::loadClouds(
    const int maxClouds,
    const int skipClouds, 
    vector<o3::PointCloud> * clouds)
{
  assert(imagePaths.size() == depthPaths.size());
  o3::PointCloud reconstruction;
  if (depthPaths.size() == 0)
    return reconstruction;
  clouds->reserve(min(maxClouds, (int)depthPaths.size()/(skipClouds+1)));
  int count = 0;
  for(size_t i=0; i < min((int)depthPaths.size()/(skipClouds+1), maxClouds) - skipClouds; i+=(skipClouds+1)) {
    o3::PointCloud * candidate = new o3::PointCloud();
    if (loadCloud(i, candidate)) {
      clouds->push_back(*candidate);
      //*reconstruction += *temp;
      //reconstruction += *candidate;
    } else {
      clouds->push_back(*candidate);
      count++;
    }
    //cout << reconstruction->size() << "," << candidate.size() << endl;
    if (count >= maxClouds)
      break;
  }
  printf("Rejected %i clouds\n", count);
  return reconstruction;
}
