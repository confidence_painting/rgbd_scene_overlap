#pragma once

#include <Eigen/Geometry>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "json.hpp"
#include "util.h"
#include "Dataset.h"
#include <assert.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
using namespace std;

class Scannet: public Dataset {

  fs::path dataPath;
  int subset;
  vector<string> datasetNames;

  vector<string> makeList(const fs::path & modalityPath);
  vector<string> makeFilteredList(const fs::path & modalityPath, const regex rgx);
  public:
    static Eigen::Matrix4f loadGroundTruth(const fs::path & poseFilePath);
    Scannet(const fs::path & basePath, int subset);
    int subsets;
    static float depthFactor;
    string path;
    vector<Eigen::Matrix4f> loadGroundTruths();
    vector<fs::path> loadImagePaths();
    vector<fs::path> loadCleanImagePaths();
    vector<fs::path> loadDepthPaths();
    vector<Calibration> loadCalibrations();
    string getName();
    float getDepthFactor();
};


