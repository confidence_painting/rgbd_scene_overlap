#pragma once

#include <iostream>

class mat4f {
public:
  mat4f() { }

  mat4f(
    const float& m00, const float& m01, const float& m02, const float& m03,
    const float& m10, const float& m11, const float& m12, const float& m13,
    const float& m20, const float& m21, const float& m22, const float& m23,
    const float& m30, const float& m31, const float& m32, const float& m33)
  {
    _m00 = m00;	_m01 = m01;	_m02 = m02;	_m03 = m03;
    _m10 = m10;	_m11 = m11;	_m12 = m12;	_m13 = m13;
    _m20 = m20;	_m21 = m21;	_m22 = m22;	_m23 = m23;
    _m30 = m30;	_m31 = m31;	_m32 = m32;	_m33 = m33;
  }

  void setIdentity() {
    matrix[0] = 1.0;	matrix[1] = 0.0f;	matrix[2] = 0.0f; matrix[3] = 0.0f;
    matrix[4] = 0.0f;	matrix[5] = 1.0;	matrix[6] = 0.0f; matrix[7] = 0.0f;
    matrix[8] = 0.0f;	matrix[9] = 0.0f;	matrix[10] = 1.0; matrix[11] = 0.0f;
    matrix[12] = 0.0f;	matrix[13] = 0.0f;	matrix[14] = 0.0f; matrix[15] = 1.0;
  }

  static mat4f identity() {
    mat4f res;	res.setIdentity();
    return res;
  }

  //! sets the matrix zero (or a specified value)
  void setZero(float v = 0.0f) {
    matrix[ 0] = matrix[ 1] = matrix[ 2] = matrix[ 3] = v;
    matrix[ 4] = matrix[ 5] = matrix[ 6] = matrix[ 7] = v;
    matrix[ 8] = matrix[ 9] = matrix[10] = matrix[11] = v;
    matrix[12] = matrix[13] = matrix[14] = matrix[15] = v;
  }

  static mat4f zero(float v = 0.0f) {
    mat4f res;	res.setZero(v);
    return res;
  }

  union {
    //! access matrix using a single array
    float matrix[16];
    //! access matrix using a two-dimensional array
    float matrix2[4][4];
    //! access matrix using single elements
    struct {
      float
        _m00, _m01, _m02, _m03,
        _m10, _m11, _m12, _m13,
        _m20, _m21, _m22, _m23,
        _m30, _m31, _m32, _m33;
    };
  };
};

//std::ostream& operator<<(std::ostream& os, const mat4f & c) {
  //for (size_t i = 0; i < 16; i++) {
    //if (i % 4 == 0 and i != 0) {
      //os << std::endl;
    //}
    //os << c.matrix[i] << ", ";
  //}
//}
  
