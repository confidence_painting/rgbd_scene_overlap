#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "mat4f.h"
#include "util.h"
#include "Scannet.h"
#include "json.hpp"
#include <assert.h>
#include "Dataset.h"
#include <sys/types.h>
#include <sys/stat.h>

#include <boost/tokenizer.hpp>

using json = nlohmann::json;
namespace fs = boost::filesystem;

using namespace std;

Scannet::Scannet(const fs::path & basePath, int subset) {
  // List directory of scannet items
  this->subset = subset;
  datasetNames = makeList(basePath);
  dataPath = basePath / datasetNames[subset];
  printf("Loading images from %s\n", dataPath.string().c_str());
}

string Scannet::getName() {
  return datasetNames[subset];
}

float
Scannet::getDepthFactor() {
  return 1000;
}

float Scannet::depthFactor = 1000;

vector<string>
Scannet::makeFilteredList(const fs::path & modalityPath, const regex rgx)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot create filtered list at path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  vector<string> v;
  while ((dp = readdir(dirp)) != NULL) {
    smatch matches;
    string temp_to_temp = string(dp->d_name);
    if(regex_search(temp_to_temp, matches, rgx)) {
      v.push_back(temp_to_temp);
    }
  }
  closedir(dirp);
  sort(v.begin(), v.end());

  return v;
}

vector<string>
Scannet::makeList(const fs::path & modalityPath)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot list path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  vector<string> v;
  readdir(dirp); // .
  readdir(dirp); // ..
  while ((dp = readdir(dirp)) != NULL) {
    v.push_back(dp->d_name);
  }
  closedir(dirp);
  sort(v.begin(), v.end());

  return v;
}

Eigen::Matrix4f
Scannet::loadGroundTruth(const fs::path & poseFilePath) {
  std::ifstream t(poseFilePath.string());
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  Eigen::Matrix4f out;
  
  vector <float> temp;
  //temp.reserve(4*4);
  for (std::string line;getline(t, line); ) {
    using namespace boost;
    tokenizer<escaped_list_separator<char> > tk(
       line, escaped_list_separator<char>('\\', ' ', '\"'));

    for (tokenizer<escaped_list_separator<char> >::iterator i(tk.begin());
       i!=tk.end();++i) 
    {
       temp.push_back(atof(i->c_str()));
    }
  }
  out << temp[0] , temp[1] , temp[2] ,  temp[3], 
         temp[4] , temp[5] , temp[6] ,  temp[7], 
         temp[8] , temp[9], temp[10], temp[11], 
         temp[12], temp[13], temp[14], temp[15];
  return out;
}

vector<Eigen::Matrix4f> Scannet::loadGroundTruths() {
  regex rgx ("frame-[0-9]*\\.pose\\.txt");
  vector<string> l = makeFilteredList(dataPath, rgx);
  vector<Eigen::Matrix4f> transforms;
  transforms.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath / fs::path(l.at(i));
    transforms.push_back(Scannet::loadGroundTruth(depthPath));
  }
  return transforms;
}

vector<fs::path> Scannet::loadCleanImagePaths() {
  regex rgx ("frame-[0-9]*\\.color\\.jpg");
  vector<string> l = makeFilteredList(dataPath, rgx);
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = getName() / fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> Scannet::loadImagePaths() {
  regex rgx ("frame-[0-9]*\\.color\\.jpg");
  vector<string> l = makeFilteredList(dataPath, rgx);
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath / fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> Scannet::loadDepthPaths() {
  regex rgx ("frame-[0-9]*\\.depth\\.pgm");
  vector<string> l = makeFilteredList(dataPath, rgx);
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath / fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<Calibration> Scannet::loadCalibrations() {
  ifstream f;
  f.open((dataPath / "calib_depth.txt").string());
  mat4f calib;
  f.read((char*)&calib, sizeof(mat4f));
  f.close();

  Calibration c (calib._m00, calib._m11, calib._m02, calib._m12);
  ofstream nice_calib;
  nice_calib.open((dataPath / "calib_depth_nice.txt").string(), ios::trunc);
  nice_calib << "{\n";
  nice_calib << "  \"fx\":" << c.fx << ",\n";
  nice_calib << "  \"fy\":" << c.fy << ",\n";
  nice_calib << "  \"cx\":" << c.cx << ",\n";
  nice_calib << "  \"cy\":" << c.cy << "\n";
  nice_calib << "}";
  nice_calib.close();
  assert(false);

  // Load list to get right length
  regex rgx ("frame-[0-9]*\\.depth\\.pgm");
  vector<string> l = makeFilteredList(dataPath, rgx);

  vector<Calibration> cs;
  cs.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    cs.push_back(c);
  }
  return cs;
}
