#pragma once

#include <Eigen/Geometry>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "json.hpp"
#include "Dataset.h"
#include "util.h"
#include <assert.h>

using namespace std;

class Sampler: public Dataset {

  Dataset * ref;
  vector<int> indices;
  vector<Eigen::Matrix4f> groundTruths;
  vector<fs::path> imagePaths;
  vector<fs::path> cleanImagePaths;
  vector<fs::path> depthPaths;
  vector<Calibration> calibs;
  public:
    int subsets;

    Sampler() {};

    Sampler(Dataset * ref,
            vector<int> indices, 
            vector<Eigen::Matrix4f> groundTruths,
            vector<fs::path> imagePaths,
            vector<fs::path> cleanImagePaths,
            vector<fs::path> depthPaths,
            vector<Calibration> calibs) :
          ref(ref), indices(indices), groundTruths(groundTruths), imagePaths(imagePaths),
          cleanImagePaths(cleanImagePaths), depthPaths(depthPaths), calibs(calibs)
    {
    }

    float getDepthFactor();
    fs::path path;
    string getName();
    vector<Eigen::Matrix4f> loadGroundTruths();
    vector<fs::path> loadImagePaths();
    // For exporting
    vector<fs::path> loadCleanImagePaths();
    vector<fs::path> loadDepthPaths();
    vector<Calibration> loadCalibrations();
    static void splitDataset(Dataset * ref, int testPercent, int valPercent, size_t totalSize, Sampler & trainDataset, Sampler & valDataset, Sampler & testDataset);
};
