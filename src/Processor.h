#pragma once

#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>

#include "util.h"
#include "Loader.h"
#include <Open3D/Open3D.h>
#include "overlap.h"

extern cv::Mat KERNEL7;
extern cv::Mat KERNEL5;
extern cv::Mat KERNEL3;

using namespace std;
namespace o3 = open3d::geometry;

class Processor {
  public:
    Loader loader;
    vector<o3::PointCloud> clouds;
    vector<cv::Mat> colorImages;
    vector<cv::Mat> depthImages;
    //vector<vector<pcl::Vertices> > verts;
    //vector<Cloud::Ptr> hulls;
    vector<int> removedClouds;
    int maxClouds;
    int skipClouds;
    float maxAngle;
    float minHullOverlap;
    float minAvgProjectedValues;
    float minICPConfidence;

    Processor(Loader loader, int maxClouds, int skipClouds, 
        float maxAngle, float minHullOverlap, 
        float minAvgProjectedValues, float minICPConfidence);
    
    float cloudOverlap(int c1, int c2);
    void compare(cv::Mat & dst);
    void save(string fileprefix);
    void save_paths(fs::path path);
    void dispPair(int c1, int c2);
};

