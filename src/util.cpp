#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <eigen3/Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>

#include <execinfo.h>  // for backtrace
#include <dlfcn.h>     // for dladdr
#include <cxxabi.h>    // for __cxa_demangle

// Dir stuff
#include <sys/stat.h>
#include <stdio.h>
#include <dirent.h>

#include "util.h"

using namespace std;

map<string, Calibration>
makeCalibrations()
{
  map<string, Calibration> camera_calibrations;
  camera_calibrations["def"] = Calibration(525.0, 525.0, 319.5, 239.5);
  camera_calibrations["fr1"] = Calibration(517.3, 516.5, 318.6, 255.3);
  camera_calibrations["fr2"] = Calibration(520.9, 521.0, 325.1, 249.7);
  camera_calibrations["fr3"] = Calibration(535.4, 539.2, 320.1, 247.6);
  camera_calibrations["asus"] = Calibration(544.47329, 544.47329, 320, 240);
  return camera_calibrations;
}

void BoundingBox::clipWithin(int x, int y, int w, int h){
    this->x = max(this->x, x);
    this->y = max(this->y, y);
    this->w = min(this->x+this->w, w) - this->x;
    this->h = min(this->y+this->h, h) - this->y;
}

ostream& operator<<(ostream& os, const BoundingBox b) {
  return os << "x: " << b.x << endl <<
               "y: " << b.y << endl <<
               "w: " << b.w << endl <<
               "h: " << b.h << endl;
}

ostream& operator<<(ostream& os, const Calibration c) {
  return os << "fx: " << c.fx << endl <<
               "fy: " << c.fy << endl <<
               "cx: " << c.cx << endl <<
               "cy: " << c.cy << endl;
}

std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

vector<string>
makeList(const fs::path & modalityPath)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot access path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  vector<string> v;
  while ((dp = readdir(dirp)) != NULL) {
    //string temp_to_temp = string(dp->d_name);
    //string name = string(dp->d_name);
    if (strcmp(dp->d_name, ".") and strcmp(dp->d_name, ".."))
      v.push_back(dp->d_name);
  }
  closedir(dirp);
  sort(v.begin(), v.end());

  return v;
}

//  matches for the first array
pair<vector<int>, vector<int> >
matchTimestamps(vector<int> & timestamps1, 
                vector<int> & timestamps2,
                int maxDifference)
{
  vector<int> index1;
  vector<int> index2;
  int fails = 0;
  for(uint i=uint(0); i<timestamps1.size(); i++)
  {
    int min_diff = 9999999;
    int match = -1;
    for(uint j=uint(0); j<timestamps2.size(); j++)
    {
      int diff = abs(timestamps1.at(i) - timestamps2.at(j));
      if (min_diff > diff && diff < maxDifference)
      {
        match = timestamps2.at(j);
        min_diff = diff;
      }
    }
    if (match != -1)
    {
      index1.push_back(timestamps1.at(i));
      index2.push_back(match);
    } else {
      fails++;
    }
  }
  cout << "Failed to find matches for " << fails << " pairs" << endl;
  pair<vector<int>, vector<int> > output (index1, index2);
  return output;
}

void
displayPairs(const vector<int> & l1,
             const vector<int> & l2,
             const vector<string> & color_ind,
             const string & prefix)
{
  for(uint i=0; i<l1.size(); i++)
  {
    const string p1 = prefix+color_ind.at(l1.at(i));
    const string p2 = prefix+color_ind.at(l2.at(i));
    cout << p1 << endl;
    cout << p2 << endl;
    const cv::Mat im1 = cv::imread(p1, 1);
    const cv::Mat im2 = cv::imread(p2, 1);
    cv::Mat dst;
    cv::hconcat(im1, im2, dst);
    cv::imshow("Pair", dst);
    cv::waitKey(0);
  }
}

void
displayPairs(const vector<pair<string, string> > & pairs,
             const string & prefix)
{
  for(auto pair : pairs)
  {
    const string p1 = prefix+pair.first;
    const string p2 = prefix+pair.second;
    cout << p1 << endl;
    cout << p2 << endl;
    const cv::Mat im1 = cv::imread(p1, 1);
    const cv::Mat im2 = cv::imread(p2, 1);
    cv::Mat dst;
    cv::hconcat(im1, im2, dst);
    cv::imshow("Pair", dst);
    cv::waitKey(0);
  }
}

void save_indices(
    const vector<int> & l1,
    const vector<int> & l2,
    const vector<string> & color_ind,
    const string & set_file_name,
    const string & tag,
    const string & train_path,
    const string & val_path,
    const string & test_path)
{
  vector<pair<string, string> > pairs;
  for(uint i=0; i<l1.size(); i++)
  {
    const string p1 = color_ind.at(l1.at(i));
    const string p2 = color_ind.at(l2.at(i));
    pairs.push_back(make_pair(p1, p2));
  }
  save(pairs, set_file_name, tag, train_path, val_path, test_path);
}

void save(const vector<pair<string, string> > & pairs,
          const string & set_file_name,
          const string & tag,
          const string & train_path,
          const string & val_path,
          const string & test_path)
{
  if (pairs.size() == 0) {
    return;
  }
  int test_percent = 20;
  int val_percent = 5;

  ofstream train_file;
  const string train_path_f = (train_path + set_file_name + "_" + tag + ".txt");
  train_file.open(train_path_f.c_str(), ios::out | ios::trunc);
  ofstream val_file;
  const string val_path_f = (val_path + set_file_name + "_" + tag + ".txt");
  val_file.open(val_path_f.c_str(), ios::out | ios::trunc);
  ofstream test_file;
  const string test_path_f = (test_path + set_file_name + "_" + tag + ".txt");
  test_file.open(test_path_f.c_str(), ios::out | ios::trunc);
  int train_count = 0;
  int val_count = 0;
  int test_count = 0;

  for (auto pair : pairs)
  {
    const string p1 = pair.first;
    const string p2 = pair.second;
    int r = rand() % 100 + 1;
    if (r > test_percent + val_percent)
    {
      train_file << p1 << " " << p2 << endl;
      train_file.flush();
      train_count++;
    } else if (r > test_percent) {
      val_file << p1 << " " << p2 << endl;
      val_file.flush();
      val_count++;
    } else {
      test_file << p1 << " " << p2 << endl;
      test_file.flush();
      test_count++;
    }
  }
  cout << train_path_f << " " << train_count << endl;
  cout << val_path_f << " " << val_count << endl;
  cout << test_path_f << " " << test_count << endl;
  train_file.close();
  val_file.close();
  test_file.close();
}

std::string str_backtrace(int skip)
{
	void *callstack[128];
	const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
	char buf[1024];
	int nFrames = backtrace(callstack, nMaxFrames);
	char **symbols = backtrace_symbols(callstack, nFrames);

	std::ostringstream trace_buf;
	for (int i = skip; i < nFrames; i++) {
		Dl_info info;
		if (dladdr(callstack[i], &info)) {
			char *demangled = NULL;
			int status;
			demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
			snprintf(buf, sizeof(buf), "%-3d %*0p %s + %zd\n",
					 i, 2 + sizeof(void*) * 2, callstack[i],
					 status == 0 ? demangled : info.dli_sname,
					 (char *)callstack[i] - (char *)info.dli_saddr);
			free(demangled);
		} else {
			snprintf(buf, sizeof(buf), "%-3d %*0p\n",
					 i, 2 + sizeof(void*) * 2, callstack[i]);
		}
		trace_buf << buf;

		snprintf(buf, sizeof(buf), "%s\n", symbols[i]);
		trace_buf << buf;
	}
	free(symbols);
	if (nFrames == nMaxFrames)
		trace_buf << "[truncated]\n";
	return trace_buf.str();
}
