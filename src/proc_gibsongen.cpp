#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "Gibson.h"
#include "Processor.h"
#include "Loader.h"
#include "Sampler.h"
#include <pcl/visualization/cloud_viewer.h>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int main(int argc, char * args[]) {

  po::options_description desc("Options");

  int subset;
  int maxMissingPoints;
  string base_directory;

  desc.add_options()
    ("help", "Gives you this help message")
    ("subset", po::value<int>(&subset)->default_value(0), "Subset number of dataset")
    ("base_directory", po::value<string>(&base_directory)->default_value(""), "Subset number of dataset")
    ("maxMissingPoints", po::value<int>(&maxMissingPoints)->default_value(100000), "Max number of missing points in pointcloud")
  ;

  po::positional_options_description p;
  p.add("base_directory", 1);
  p.add("subset", 2);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, args).
            options(desc).positional(p).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }

  fs::path area_path = fs::path(base_directory) / "gibson";
  fs::path index_path = fs::path(base_directory) / "indexes";

  cout << "Subset " <<subset << endl;
  printf("Looking for datasets in %s\n", area_path.string().c_str());
  Dataset *ds = new Gibson(area_path, subset);


  Loader loader(ds, 999999, 0, 0);

  vector<Cloud::Ptr> clouds;
  Cloud::Ptr reconstruction = loader.loadClouds(999999, 0, &clouds);

  pcl::io::savePCDFileASCII ("test_pcd.pcd", *reconstruction);

  cout << reconstruction->size() << endl;;
  pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
  viewer.showCloud (reconstruction);
  while (!viewer.wasStopped ()) { }
}



