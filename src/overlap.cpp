#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <Open3D/Open3D.h>

#include "overlap.h"
#include "util.h"

cv::Mat KERNEL7 = cv::Mat::ones(7, 7, CV_32FC1);
cv::Mat KERNEL5 = cv::Mat::ones(5, 5, CV_32FC1);
cv::Mat KERNEL3 = cv::Mat::ones(3, 3, CV_32FC1);

float
bbx_overlap(
    const cv::Mat & backprojected,
    const BoundingBox & bb)
{
  bool DISPLAY = false;
  if (bb.h == 0 || bb.w == 0) {
    return 0;
  }

  cv::Mat backproj_denoised;
  cv::threshold(backprojected, backproj_denoised, 1e-10, 1, 0);
  cv::Mat bbd = cv::Mat::zeros(cv::Size(bb.w, bb.h), CV_32FC1);
  backproj_denoised(cv::Rect_<int>(bb.x, bb.y, bb.w, bb.h)).copyTo(bbd);
  float union_area = cv::sum(backproj_denoised)[0] + bb.w*bb.h - cv::sum(bbd)[0];
  if (DISPLAY) {
    // float size = backproj_denoised.rows;
    cv::Mat canvas;
    backproj_denoised.copyTo(canvas);
    // draw_blobs(canvas, {at::tensor({bb.x/size+bb.w/size/2, bb.y/size+bb.h/size/2, bb.w/size, bb.h/size})});
    cv::imshow("Backproj_denoised", canvas);
    // std::cout << backproj_denoised << std::endl;
    cv::imshow("Backproj", backprojected);
    // std::cout << cv::sum(bbd)[0] << std::endl;
    // std::cout << union_area << std::endl;
    // std::cout << cv::sum(backproj_denoised)[0] << std::endl;
    // std::cout << bb.w*bb.h << std::endl;
    std::cout << "IOU: " << cv::sum(bbd)[0] / union_area << std::endl;
    cv::waitKey(0);
  }

  // Take the IOU
  return cv::sum(bbd)[0] / union_area;
  // return cv::mean(bbd)[0];
}

// project 2 into 1
float
back_project(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const o3::PointCloud & cand_pc,
    const Eigen::Matrix4f & query_pose,
    const Eigen::Matrix4f & cand_pose,
    const Calibration & query_calib,
    const Calibration & cand_calib,
    const BoundingBox & query_bbx,
    const BoundingBox & cand_bbx,
    const float depthFactor,
    cv::Mat & backproj)
{
  // Generate clouds
  open3d::geometry::PointCloud pc = cand_pc;

  // Need to add the bounding box here
  // Check bounds when iterating
  // Only project within bounding box 2, then check output on bounding boxes2
  //pc = depthToCloud(cand_depth, cand_calib, depthFactor, cand_bbx);
  Eigen::Matrix4f t = query_pose.inverse()*cand_pose;
  pc.Transform(t.cast<double>());

  // Transform pointclouds
  //std::vector<std::shared_ptr<const open3d::geometry::Geometry> > geoms;
  //std::shared_ptr<const open3d::geometry::PointCloud> pc_ptr (&pc);
  //geoms.push_back(pc_ptr);
  //open3d::visualization::DrawGeometries(geoms, "Backproj", 500, 500, 250, 250);

  // Only transform hull points

  cv::Mat cand_reproj;
  colorCloudToMat(cand_reproj, pc, cand_calib, cand_depth.cols, cand_depth.rows, depthFactor);
  cv::dilate(cand_reproj, cand_reproj, KERNEL3);

  // If 1 is in front of 2, then mask out 2
  // Remove occlusion
  cv::Mat query_scaled;
  query_depth.convertTo(query_scaled, CV_32FC1, 1);
  cv::Mat mask = cv::abs(query_scaled - cand_reproj) <= depthFactor*0.35; // don't forget about the depth factor

  cand_reproj.copyTo(backproj, mask);

  // Crop to bounding box in image 1  
  return bbx_overlap(backproj, query_bbx);
}

// project 2 into 1
float
back_project(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const Eigen::Matrix4f & query_pose,
    const Eigen::Matrix4f & cand_pose,
    const Calibration & query_calib,
    const Calibration & cand_calib,
    const BoundingBox & query_bbx,
    const BoundingBox & cand_bbx,
    const float depthFactor,
    cv::Mat & backproj)
{
  // Generate clouds
  open3d::geometry::PointCloud pc;

  // Need to add the bounding box here
  // Check bounds when iterating
  // Only project within bounding box 2, then check output on bounding boxes2
  Eigen::Matrix4d t = (query_pose.inverse()*cand_pose).cast<double>();
  pc = depthToCloud(cand_depth, NULL, cand_calib, depthFactor, cand_bbx, true, 1);
  pc.Transform(t);

  // Transform pointclouds
  //std::vector<std::shared_ptr<const open3d::geometry::Geometry> > geoms;
  //std::shared_ptr<const open3d::geometry::PointCloud> pc_ptr (&pc);
  //geoms.push_back(pc_ptr);
  //open3d::visualization::DrawGeometries(geoms, "Backproj", 500, 500, 250, 250);

  // Only transform hull points

  cv::Mat cand_reproj;
  colorCloudToMat(cand_reproj, pc, cand_calib, cand_depth.cols, cand_depth.rows, depthFactor);
  cv::dilate(cand_reproj, cand_reproj, KERNEL3);

  // If 1 is in front of 2, then mask out 2
  // Remove occlusion
  cv::Mat query_scaled;
  query_depth.convertTo(query_scaled, CV_32FC1, 1);
  cv::Mat mask = cv::abs(query_scaled - cand_reproj) <= depthFactor*0.35; // don't forget about the depth factor

  cand_reproj.copyTo(backproj, mask);

  // Crop to bounding box in image 1  
  return bbx_overlap(backproj, query_bbx);
}

open3d::geometry::PointCloud 
depthToCloud(
    const cv::Mat & depth,
    const cv::Mat * color,
    const Calibration calib,
    const float factor,
    const BoundingBox & bb,
    const bool interp_cloud,
    const int decimation_factor)
{
  // At one meter, with our camera, 1 meter is 512 pixels
  float pixels_per_meter = 32;
  open3d::geometry::PointCloud out;
  out.points_.reserve(depth.cols*depth.rows);
  if (color != NULL)
    out.colors_.reserve(depth.cols*depth.rows);
  for(int w = bb.x; w < std::min(int(bb.x+bb.w), depth.cols); w+=decimation_factor)
  {
    for(int h = bb.y; h < std::min(int(bb.y+bb.h), depth.rows); h+=decimation_factor)
    {
      float dep = float(depth.at<ushort>(h, w)) / factor;
      if (dep > 0.2f && dep < 10)
      {
        // Calculate how wide the pixel is to interpolate the missing area
        // Round up so there is always at least one pixel

        if (!interp_cloud) {
          Eigen::Vector3d pt;
          pt[0] = float(w - calib.cx) * dep / calib.fx;
          pt[1] = float(h - calib.cy) * dep / calib.fy;
          pt[2] = dep;
          out.points_.push_back(pt);
          if (color != NULL) {
            Eigen::Vector3d c;
            // c << double(color->at<float>(h, w)), 0, 0;
            c << double(color->at<float>(h, w)) * dep * dep, 0, 0;
            out.colors_.push_back(c);
          }
        } else {
          // Get depth values for surrounding points
          float dep_up = float(depth.at<ushort>(std::max(h-1, 0), w)) / factor;
          float dep_down = float(depth.at<ushort>(std::min(h+1, depth.rows-1), w)) / factor;
          float dep_right = float(depth.at<ushort>(h, std::min(w+1, depth.cols-1))) / factor;
          float dep_left = float(depth.at<ushort>(h, std::max(w-1, 0))) / factor;
          dep_up = (dep_up != 0) ? dep_up : dep;
          dep_down = (dep_down != 0) ? dep_down : dep;
          dep_right = (dep_right != 0) ? dep_right : dep;
          dep_left = (dep_left != 0) ? dep_left : dep;

          float x = float(w - calib.cx) * dep / calib.fx;
          float y = float(h - calib.cy) * dep / calib.fy;

          // Get distance to surrounding points
          float dist_up = sqrt(
              (dep_up - dep)*(dep_up - dep) +
              pow(y - float(h-1-calib.cy)*dep_up/calib.fy, 2));
          float dist_down = sqrt(
              (dep_down - dep)*(dep_down - dep) +
              pow(y - float(h+1-calib.cy)*dep_down/calib.fy, 2));
          float dist_right = sqrt(
              (dep_right - dep)*(dep_right - dep) +
              pow(x - float(w+1-calib.cx)*dep_right/calib.fx, 2));
          float dist_left = sqrt(
              (dep_left - dep)*(dep_left - dep) +
              pow(x - float(w+1-calib.cx)*dep_left/calib.fx, 2));

          // Check if point is isolated
          // Distance between pixels grows as depth to pixel grows
          // This erodes edges but it's necessary
          if ((dist_up > 1/dep/2 ||
               dist_down > 1/dep/2) ||
              (dist_right > 1/dep/2 ||
               dist_left > 1/dep/2)) {
            // Point is isolated
            continue;
          }

          // Now calculate how many points are needed to interpolate
          // At 1 meter, 1 meter wide = calib.fx*2 number of pixels
          // Can be reduced to save on CPU time
          int pixel_w = std::max(
              std::min(
                (dist_up+dist_down)*pixels_per_meter,
                20.0f),
              1.0f);
          int pixel_h = std::max(
              std::min(
                (dist_right+dist_left)*pixels_per_meter,
                20.0f),
              1.0f);
          // TODO for loop for each side
          // printf("dist_up: %f\n", dist_up);
          // printf("dist_down: %f\n", dist_down);
          // printf("dist_right: %f\n", dist_right);
          // printf("dist_left: %f\n", dist_left);
          // printf("dep: %f\n", dep);
          // printf("pixel_w: %i\n", pixel_w);
          // printf("pixel_h: %i\n", pixel_h);

          for (int w_sub = 0; w_sub < pixel_w; w_sub++) {
            for (int h_sub = 0; h_sub < pixel_h; h_sub++) {
              // Piecewise depth interpolation
              float sub_pix_x = float(w_sub)/pixel_w;
              float sub_pix_y = float(h_sub)/pixel_h;
              float dep_horz_interp;
              if (w_sub <= dep/calib.fx/2) {
                // Interpolate using left point
                dep_horz_interp = dep*(sub_pix_x*2) + dep_left*(1-sub_pix_x*2);
              } else {
                // Interpolate using right point
                dep_horz_interp = dep*(2-sub_pix_x*2) + dep_right*(sub_pix_x*2 - 1);
              }

              float dep_vert_interp;
              if (h_sub <= dep/calib.fy/2) {
                // Interpolate using up point
                dep_vert_interp = dep*(sub_pix_y*2) + dep_up*(1-sub_pix_y*2);
              } else {
                // Interpolate using down point
                dep_vert_interp = dep*(2-sub_pix_y*2) + dep_down*(sub_pix_y*2 - 1);
              }

              float dep_interp = (dep_vert_interp+dep_horz_interp)/2;
              Eigen::Vector3d pt (float((sub_pix_x+w) - calib.cx) * dep_interp / calib.fx,
                                  float((sub_pix_y+h) - calib.cy) * dep_interp / calib.fy,
                                  dep_interp);
              out.points_.push_back(pt);
              if (color != NULL) {
                Eigen::Vector3d c (double(color->at<float>(h, w)) * dep_interp * dep_interp, 0, 0);
                // c << double(color->at<float>(h, w)), 0, 0;
                out.colors_.push_back(c);
              }
            }
          }
          // END INTERP
        }
      }
      // ELSE NOTHING
    }
  }
  return out;
}

void
colorCloudToMat(
    cv::Mat & dst,
    const o3::PointCloud & cloud,
    const Calibration & calib,
    int width, int height, float factor)
{
  dst = cv::Mat(cv::Size(height, width), CV_32FC1, cv::Scalar(0));
  for (size_t i=0; i<cloud.points_.size(); i++) {
    float x = calib.fx*cloud.points_[i][0]/cloud.points_[i][2] + calib.cx;
    float y = calib.fy*cloud.points_[i][1]/cloud.points_[i][2] + calib.cy;
    float ix = int(x+0.5);
    float iy = int(y+0.5);
    if (ix >= width || iy >= height || ix < 0 || iy < 0)
      continue;
    if (cloud.points_[i][2] > 0)
      dst.at<float>(iy, ix) = (float)cloud.points_[i][2] * factor;
  }
}
