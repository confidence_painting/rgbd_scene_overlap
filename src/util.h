#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <string>
#include <vector>
#include <utility>
#include <boost/filesystem.hpp>
#include <map>

namespace fs = boost::filesystem;

using namespace std;

enum DatasetType {
  FREIBURG = 0,
  SCENENN = 1,
  STANFORD = 2,
};

struct Calibration
{
  Calibration( float fx, float fy, float cx, float cy ) : fx(fx), fy(fy), cx(cx), cy(cy) {}
  Calibration() : fx(0), fy(0), cx(0), cy(0) {}
  float fx;
  float fy;
  float cx;
  float cy;
};

struct BoundingBox
{
  BoundingBox( int x, int y, int w, int h ) : x(x), y(y), w(w), h(h) {}
  BoundingBox() : x(0), y(0), w(0), h(0) {}
  // Clip to be within image
  void clipWithin(int x, int y, int w, int h);
  int x, y, w, h;
};

ostream& operator<<(ostream& os, const Calibration c);
ostream& operator<<(ostream& os, const BoundingBox c);

map<string, Calibration> makeCalibrations();

static const map<string, Calibration> CAMERA_CALIBRATIONS = makeCalibrations();

std::string type2str(int type);

vector<string>
makeList(const fs::path & modalityPath);

void
displayPairs(const vector<int> & l1,
             const vector<int> & l2,
             const vector<string> & color_ind,
             const string & prefix);

void
displayPairs(const vector<pair<string, string> > & pairs,
             const string & prefix);

pair<vector<int>, vector<int> >
matchTimestamps(vector<int> & timestamps1, 
                vector<int> & timestamps2,
                int maxDifference);

void save_indices(
    const vector<int> & l1,
    const vector<int> & l2,
    const vector<string> & color_ind,
    const string & set_file_name,
    const string & tag,
    const string & train_path,
    const string & val_path,
    const string & test_path);

void save(const vector<pair<string, string> > & pairs,
          const string & set_file_name,
          const string & tag,
          const string & train_path,
          const string & val_path,
          const string & test_path);

std::string str_backtrace(int skip = 1);
