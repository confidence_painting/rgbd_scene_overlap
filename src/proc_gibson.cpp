#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "Gibson.h"
#include "Processor.h"
#include "Loader.h"
#include "Sampler.h"
#include "util.h"

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  cout << str_backtrace() << endl;
  exit(1);
}

int main(int argc, char * args[]) {

  po::options_description desc("Options");
  int subset;
  int maxMissingPoints;
  int decimation_factor;
  float minEntropy;
  int maxClouds;
  int skipClouds;
  float maxAngle;
  float minHullOverlap;
  float minAvgProjectedValues;
  float minICPConfidence;
  
  string base_directory;
  string dataset_name;
  desc.add_options()
    ("help", "Gives you this help message")
    ("subset", po::value<int>(&subset)->default_value(0), "Subset number of dataset")
    ("base_directory", po::value<string>(&base_directory)->default_value(""), "Subset number of dataset")
    ("maxMissingPoints", po::value<int>(&maxMissingPoints)->default_value(100000), "Max number of missing points in pointcloud")
    ("decimation_factor", po::value<int>(&decimation_factor)->default_value(10), "Number of pixels to skip when decimating pointcloud")
    ("minEntropy", po::value<float>(&minEntropy)->default_value(5), "Minimum image entropy for loading pointcloud")
    ("maxClouds", po::value<int>(&maxClouds)->default_value(999999), "Maximum of pointclouds to be loaded")
    ("skipClouds", po::value<int>(&skipClouds)->default_value(0), "How many point clouds to skip in between viewpoints")
    ("maxAngle", po::value<float>(&maxAngle)->default_value(3*M_PI/4), "Max angle between view points in radians")
    ("minHullOverlap", po::value<float>(&minHullOverlap)->default_value(0.4), "Min mutual percentage hull overlap between clouds")
    ("minAvgProjectedValues", po::value<float>(&minAvgProjectedValues)->default_value(0.3), "Minimum average value of projected image. Read the src.")
    ("minICPConfidence", po::value<float>(&minICPConfidence)->default_value(0.2), "Minimum ICP confidence between pairs")
  ;

  po::positional_options_description p;
  p.add("base_directory", 1);
  p.add("subset", 2);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, args).
            options(desc).positional(p).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }

  fs::path area_path = fs::path(base_directory) / "gibson";
  fs::path index_path = fs::path(base_directory) / "indexes";

  cout << "Subset " <<subset << endl;
  printf("Looking for datasets in %s\n", area_path.string().c_str());

  Dataset *ds = new Gibson(area_path, subset);
  //Sampler trainDataset, valDataset, testDataset;
  //Sampler::splitDataset(ds, 0, 0, maxClouds, trainDataset, valDataset, testDataset);
  
  string tag = "train";

  //auto proc = [&maxMissingPoints, &decimation_factor, minEntropy, &maxClouds, &skipClouds, &maxAngle, &minHullOverlap, &minAvgProjectedValues, &minICPConfidence, &index_path]
    //(Dataset * ds, string tag) {

    signal(SIGSEGV, handler); 
    Loader loader(ds, maxMissingPoints, decimation_factor, minEntropy);
    Processor proc (loader, maxClouds, skipClouds, maxAngle, minHullOverlap, 
        minAvgProjectedValues, minICPConfidence);
    cv::Mat gt;
    proc.compare(gt);
    // string gt_path = (index_path / tag / fs::path(ds->getName() + "_gt.png")).string();
    string gt_path = (index_path / tag / fs::path(ds->getName() + "_gt.tiff")).string();
    cout << "Saving gt image to " << gt_path << endl;
    cv::imwrite(gt_path, gt);
    gt_path = (index_path / tag / fs::path(ds->getName() + "_gt.exr")).string();
    cout << "Saving gt image to " << gt_path << endl;
    cv::imwrite(gt_path, gt);
    proc.save_paths(index_path / tag / fs::path(ds->getName() + "_paths.txt"));
  //};

  //proc(&trainDataset, "train");
  //proc(&valDataset, "val");
  //proc(&testDataset, "test");
    return 0;
}
