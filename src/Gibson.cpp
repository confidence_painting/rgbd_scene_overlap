#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "mat4f.h"
#include "util.h"
#include "Gibson.h"
#include "json.hpp"
#include <assert.h>
#include "Dataset.h"

using json = nlohmann::json;
namespace fs = boost::filesystem;

using namespace std;

Gibson::Gibson(const fs::path & basePath, int subset) {
  // List directory of scannet items
  this->subset = subset;
  datasetNames = makeList(basePath);
  dataPath = basePath / datasetNames[subset];
  printf("Loading images from %s\n", dataPath.string().c_str());
}

string Gibson::getName() {
  return datasetNames[subset];
}

float
Gibson::getDepthFactor() {
  return 512;
}

float Gibson::depthFactor = 512;

Eigen::Matrix4f
Gibson::loadGroundTruth(const fs::path & poseFilePath) {
  std::ifstream t(poseFilePath.string());
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());
  json js = json::parse(str);
  float s = 1;
  Eigen::Translation3f translation (s*float(js["position"][0]), s*float(js["position"][1]), s*float(js["position"][2]));
  //Eigen::Translation3f translation (s*float(js["position"][0]), s*float(js["position"][1]), float(0));
  //Eigen::Translation3f translation (float(0), s*float(js["position"][1]), s*float(js["position"][2]));
  //Eigen::Translation3f translation (0,0,0);
  Eigen::Quaternionf q(js["orientation"][3],
                       js["orientation"][0],
                       js["orientation"][1],
                       js["orientation"][2]);
  // Translate, then rotate
  //coc = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());
  //coc = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX());

  // Input cloud: x left, y up, z front
  // Input transform: x left, y front, z up
  //coc = 
      //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());
  // Translate to ROS Coords
  Eigen::Quaternionf corr;
  corr = 
      Eigen::AngleAxisf(-M_PI/2, Eigen::Vector3f::UnitX()) * 
      Eigen::AngleAxisf(M_PI/2, Eigen::Vector3f::UnitY()) *
      Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY());
  //corr = Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());

  // Apply the correction from PyBullet -> ROS
  //Eigen::Quaternionf corr;
  //corr = Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());

  return (translation * q * corr).matrix();
  //return (translation * q).matrix();
}

vector<Eigen::Matrix4f> Gibson::loadGroundTruths() {
  vector<string> l = makeList(dataPath / "metadata");
  vector<Eigen::Matrix4f> transforms;
  transforms.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath / "metadata" / fs::path(l.at(i));
    transforms.push_back(Gibson::loadGroundTruth(depthPath));
  }
  return transforms;
}

vector<fs::path> Gibson::loadCleanImagePaths() {
  vector<string> l = makeList(dataPath / "rgb");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = fs::path(getName()) / "rgb" / l.at(i);
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> Gibson::loadImagePaths() {
  vector<string> l = makeList(dataPath / "rgb");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath /  "rgb" /fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<fs::path> Gibson::loadDepthPaths() {
  vector<string> l = makeList(dataPath / "depth");
  vector<fs::path> paths;
  paths.reserve(l.size());
  for (size_t i=0; i < l.size(); i++) {
    const fs::path depthPath = dataPath /  "depth" /fs::path(l.at(i));
    paths.push_back(depthPath);
  }
  return paths;
}

vector<Calibration> Gibson::loadCalibrations() {

  // fx fy cx cy
  Calibration c (128, 128, 128, 128);
  // Calibration c (256, 256, 256, 256);
  vector<string> _l = makeList(dataPath / "depth"); // Load to get size

  vector<Calibration> cs;
  cs.reserve(_l.size());
  for (size_t i=0; i < _l.size(); i++) {
    cs.push_back(c);
  }
  return cs;
}
