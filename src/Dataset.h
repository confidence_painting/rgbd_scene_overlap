#pragma once

#include <Eigen/Geometry>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "json.hpp"
#include <assert.h>
#include "util.h"
#include <boost/filesystem.hpp>

using namespace std;
namespace fs = boost::filesystem;

class Dataset {
  public:
    fs::path dataPath;
    int subsets;
    virtual float getDepthFactor() = 0;
    fs::path path;
    Dataset() {};
    ~Dataset() {};
    virtual string getName() = 0;
    virtual vector<Eigen::Matrix4f> loadGroundTruths() = 0;
    virtual vector<fs::path> loadImagePaths() = 0;
    // For exporting. Relative paths
    virtual vector<fs::path> loadCleanImagePaths() = 0;
    virtual vector<fs::path> loadDepthPaths() = 0;
    virtual vector<Calibration> loadCalibrations() = 0;
};
