#include <zmq.hpp>
#include <iostream>
#include "json.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "util.h"
#include "Gibson.h"
#include "overlap.h"

using json = nlohmann::json;
namespace fs = boost::filesystem;
const float eps = 0.0001;

int main(int argc, char * args[]) {
  zmq::context_t context = zmq::context_t();

  zmq::socket_t sock(context, zmq::socket_type::rep);
  sock.setsockopt( ZMQ_RCVTIMEO, 5000 );
  sock.bind("tcp://*:5556");

  // zmq::socket_t sub(context, zmq::socket_type::rep);
  // sub.setsockopt(ZMQ_SUBSCRIBE, topic);
  // sub.connect("tcp://localhost:5557");
  // assert(sub.connected());

  while (true) {
    // Recieve bounding boxes and image paths
    zmq::message_t request;
    sock.recv(&request);
    std::string msg_str(static_cast<char*>(request.data()), request.size());
    json mess;
    try {
      mess = json::parse(msg_str);
    } catch (...) {
      std::cout << "Malformed request: " << msg_str << std::endl;
      continue;
    }

    // Load clouds
    //float p = mess["perturbation"];
    cv::Mat dep1 = cv::imread(mess["depth_path1"], -1);
    cv::Mat dep2 = cv::imread(mess["depth_path2"], -1);
    fs::path pose_path1 = mess["pose_path1"].get<std::string>();
    fs::path pose_path2 = mess["pose_path2"].get<std::string>();
    Eigen::Matrix4f pose1 = Gibson::loadGroundTruth(pose_path1);
    Eigen::Matrix4f pose2 = Gibson::loadGroundTruth(pose_path2);

    Calibration c1 ( mess["calib1"]["fx"].get<float>(),
                     mess["calib1"]["fy"].get<float>(),
                     mess["calib1"]["cx"].get<float>(),
                     mess["calib1"]["cy"].get<float>());
    Calibration c2 ( mess["calib2"]["fx"].get<float>(),
                     mess["calib2"]["fy"].get<float>(),
                     mess["calib2"]["cx"].get<float>(),
                     mess["calib2"]["cy"].get<float>());

    // Run check to see if there is any overlap at all
    BoundingBox full(0, 0, 256, 256);
    cv::Mat backproj;
    float ov = back_project(dep1, dep2,
                            pose1, pose2,
                            c1, c2,
                            full, full,
                            Gibson::depthFactor, backproj);

    // Remove clouds with few points
    auto boxes1 = mess["boxes1"];
    auto boxes2 = mess["boxes2"];
    // Preallocate this
    size_t num_out = min(boxes1.size(), boxes2.size());
    std::vector<float> overlaps (num_out, 0);

    // derivative bounding boxes
    std::vector<float> overlaps_inc_top (num_out, 0);
    std::vector<float> overlaps_dec_top (num_out, 0);

    std::vector<float> overlaps_inc_bot (num_out, 0);
    std::vector<float> overlaps_dec_bot (num_out, 0);

    std::vector<float> overlaps_inc_left (num_out, 0);
    std::vector<float> overlaps_dec_left (num_out, 0);

    std::vector<float> overlaps_inc_right (num_out, 0);
    std::vector<float> overlaps_dec_right (num_out, 0);

    if (ov < eps) {
      std::cout << "No overlap, shortcutting" << std::endl;
    } else {
#pragma omp parallel for
      for (size_t i=0; i<num_out; i++) {
        BoundingBox bb1(boxes1[i]["x"].get<float>()*dep1.cols + 0.5,
                        boxes1[i]["y"].get<float>()*dep1.rows + 0.5,
                        boxes1[i]["w"].get<float>()*dep1.cols + 0.5,
                        boxes1[i]["h"].get<float>()*dep1.rows + 0.5);
        BoundingBox bb2(boxes2[i]["x"].get<float>()*dep2.cols + 0.5,
                        boxes2[i]["y"].get<float>()*dep2.rows + 0.5,
                        boxes2[i]["w"].get<float>()*dep2.cols + 0.5,
                        boxes2[i]["h"].get<float>()*dep2.rows + 0.5);
        // Correct bbxes so they do not exceed bounds
        bb1.clipWithin(0, 0, dep1.cols, dep1.rows);
        bb2.clipWithin(0, 0, dep2.cols, dep2.rows);
        bool zerodim = (bb1.w <= 0 or bb1.h <= 0 or bb2.w <= 0 or bb2.h <= 0);
        // Run prelim test to see if there is even overlap in full frame image
        if (zerodim or bbx_overlap(backproj, bb1) < eps) {
          overlaps[i] = 0;
          overlaps_inc_top[i] = 0;
          overlaps_dec_top[i] = 0;

          overlaps_inc_bot[i] = 0;
          overlaps_dec_bot[i] = 0;

          overlaps_inc_left[i] = 0;
          overlaps_dec_left[i] = 0;

          overlaps_inc_right[i] = 0;
          overlaps_dec_right[i] = 0;
        } else {
          cv::Mat backproj_bbx;
          // overlaps[i] = back_project(dep1, dep2,
          //                   pose1, pose2,
          //                   c1, c2,
          //                   bb1, bb2,
          //                   Gibson::depthFactor, backproj_bbx);
          // Now get derivative bounding boxes
          /*
          overlaps_inc_top[i] = perturb_overlap(backproj_bbx, bb1, 0, -p, 0, p);
          overlaps_dec_top[i] = perturb_overlap(backproj_bbx, bb1, 0, p, 0, -p);

          overlaps_inc_bot[i] = perturb_overlap(backproj_bbx, bb1, 0, 0, 0, p);
          overlaps_dec_bot[i] = perturb_overlap(backproj_bbx, bb1, 0, 0, 0, -p);

          overlaps_inc_left[i] = perturb_overlap(backproj_bbx, bb1, p, 0, -p, 0);
          overlaps_dec_left[i] = perturb_overlap(backproj_bbx, bb1, -p, 0, p, 0);

          overlaps_inc_right[i] = perturb_overlap(backproj_bbx, bb1, 0, 0, p, 0);
          overlaps_dec_right[i] = perturb_overlap(backproj_bbx, bb1, 0, 0, -p, 0);
          */
        }
      }
    }


    json resp;
    // Find true overlap
    resp["overlaps"] = overlaps;
    /*
    resp["overlaps_inc_top"] = overlaps_inc_top;
    resp["overlaps_dec_top"] = overlaps_dec_top;

    resp["overlaps_inc_bot"] = overlaps_inc_bot;
    resp["overlaps_dec_bot"] = overlaps_dec_bot;

    resp["overlaps_inc_left"] = overlaps_inc_left;
    resp["overlaps_dec_left"] = overlaps_dec_left;

    resp["overlaps_inc_right"] = overlaps_inc_right;
    resp["overlaps_dec_right"] = overlaps_dec_right;
    */

    // Send back overlap data
    std::string out = resp.dump();
    zmq::message_t response(out.size());
    memcpy(response.data(), out.c_str(), out.size());
    std::cout << out.size() << std::endl;
    sock.send(response);
  }
}
