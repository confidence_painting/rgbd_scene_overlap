#include <Eigen/Geometry>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "json.hpp"
#include "Dataset.h"
#include "Sampler.h"
#include "util.h"
#include <assert.h>

using namespace std;

void Sampler::splitDataset(Dataset * ref,
    int testPercent, int valPercent, size_t totalSize,
    Sampler & trainDataset, Sampler & valDataset, Sampler & testDataset)
{
  vector<int> trainIndices, testIndices, valIndices;
  vector<Eigen::Matrix4f> groundTruths = ref->loadGroundTruths();
  vector<fs::path> imagePaths = ref->loadImagePaths();
  vector<fs::path> cleanImagePaths = ref->loadCleanImagePaths();
  vector<fs::path> depthPaths = ref->loadDepthPaths();
  vector<Calibration> calibs = ref->loadCalibrations();

  for (size_t i=0; i<min(totalSize, calibs.size()); i++) {
    int r = rand() % 100 + 1;
    if (r > testPercent + valPercent)
      trainIndices.push_back(i);
    else if (r > testPercent)
      testIndices.push_back(i);
    else
      valIndices.push_back(i);
  }
  cout << "Train size: " << trainIndices.size() << endl;
  cout << "Test size: " << testIndices.size() << endl;
  cout << "Val size: " << valIndices.size() << endl;

  trainDataset = Sampler(ref, trainIndices, groundTruths, imagePaths, 
                         cleanImagePaths, depthPaths, calibs);
  testDataset = Sampler(ref, testIndices, groundTruths, imagePaths, 
                        cleanImagePaths, depthPaths, calibs);
  valDataset = Sampler(ref, valIndices, groundTruths, imagePaths, 
                       cleanImagePaths, depthPaths, calibs);
}

float Sampler::getDepthFactor() {
  return ref->getDepthFactor();
}

string Sampler::getName() {
  return ref->getName();
}

vector<Eigen::Matrix4f>
Sampler::loadGroundTruths() {
  vector<Eigen::Matrix4f> out;
  out.reserve(indices.size());
  for (auto i : indices) {
    out.push_back(groundTruths[i]);
  }
  return out;
}

vector<fs::path>
Sampler::loadImagePaths() {
  vector<fs::path> out;
  out.reserve(indices.size());
  for (auto i : indices) {
    out.push_back(imagePaths[i]);
  }
  return out;
}

// For exporting
vector<fs::path>
Sampler::loadCleanImagePaths() {
  vector<fs::path> out;
  out.reserve(indices.size());
  for (auto i : indices) {
    out.push_back(cleanImagePaths[i]);
  }
  return out;
}

vector<fs::path>
Sampler::loadDepthPaths() {
  vector<fs::path> out;
  out.reserve(indices.size());
  for (auto i : indices) {
    out.push_back(depthPaths[i]);
  }
  return out;
}

vector<Calibration>
Sampler::loadCalibrations() {
  vector<Calibration> out;
  out.reserve(indices.size());
  for (auto i : indices) {
    out.push_back(calibs[i]);
  }
  return out;
}

