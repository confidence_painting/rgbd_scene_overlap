#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "util.h"
#include "Processor.h"

#include "overlap.h"

const float eps = 0.0001;

Processor::Processor(Loader loader, int maxClouds, int skipClouds, 
        float maxAngle, float minHullOverlap, 
        float minAvgProjectedValues, float minICPConfidence) :
  loader(loader),
  maxClouds(maxClouds),
  skipClouds(skipClouds),
  maxAngle(maxAngle),
  minHullOverlap(minHullOverlap),
  minAvgProjectedValues(minAvgProjectedValues),
  minICPConfidence(minICPConfidence)
{
  cout << "Loading images and clouds" << endl;
  size_t size = min(maxClouds, (int)loader.depthPaths.size());
  // colorImages.reserve(size);
  // depthImages.reserve(size);
  depthImages = std::vector<cv::Mat>(size);
  clouds = std::vector<open3d::geometry::PointCloud>(size);
  // clouds.reserve(size);

  cv::Mat depth = cv::imread(loader.depthPaths[0].string(), -1);
  BoundingBox full(0, 0, depth.cols, depth.rows);

#pragma omp parallel for
  for (size_t i=0; i<size; i++) {
    cv::Mat dep = cv::imread(loader.depthPaths[i].string(), -1);
    cv::resize(dep, dep, cv::Size(), 0.5, 0.5);
    depthImages[i] = dep;
    // cv::Mat col = cv::imread(loader.imagePaths[i].string(), -1);
    // cv::resize(col, col, cv::Size(), 0.5, 0.5);
    // colorImages.push_back(col);
    clouds[i] = depthToCloud( dep,
        NULL,
        loader.calibs[i],
        loader.ds->getDepthFactor(),
        full,
        true,
        1);//loader.decimation_factor));//, colorImages[i]));
  }
  //cout << "Loading clouds" << endl;
  //o3::PointCloud reconstruction = loader.loadClouds(maxClouds, skipClouds, &clouds);
  // Save point cloud for viewing
  //fs::path pointcloud_path = loader.ds->dataPath / "full_pointcloud.pcd";
  //open3d::io::WritePointCloudToPCD(pointcloud_path.string(), reconstruction, false, true);
  //cout << "Saved point cloud to " << pointcloud_path << endl;
  // Preprocess hulls
  //printf("Found %i clouds\n", int(clouds.size()));
  /*
  cout << "Creating hulls" << endl;
  for(uint i=0; i<clouds.size(); i++)
  {
    pcl::ConvexHull<pcl::PointXYZRGB> hull;
    vector<pcl::Vertices> polys;
    Cloud::Ptr surfacePoints (new Cloud());
    //cout << "cloud ptr: " << clouds[i] << endl;
    if (clouds[i]) {
      //cout << "HI" << endl;
      //cout << "size: " << clouds[i]->size() << endl;
      //cout << "cloud: " << *clouds[i] << endl;
      //for (size_t j=0; j<clouds[i]->size(); j++) {
        //cout << j << flush;
        //cout << "cloud: " << *clouds[i] << flush;
        //auto p = clouds[i]->at(j);
        //cout << p << flush;
      //}
      hull.setInputCloud(clouds[i]);
      hull.reconstruct(*surfacePoints, polys);
    }
    verts.push_back(polys);
    hulls.push_back(surfacePoints);
  }
  */
}

bool
cross2(Eigen::Vector4f pa,
       Eigen::Vector4f pb,
       Eigen::Vector4f pc,
       Eigen::Vector4f p0,
       Eigen::Vector4f p1,
       Eigen::Vector4f p2)
{
  float dXa = pa(0, 0) - p2(0, 0);
  float dYa = pa(1, 0) - p2(1, 0);
  float dXb = pb(0, 0) - p2(0, 0);
  float dYb = pb(1, 0) - p2(1, 0);
  float dXc = pc(0, 0) - p2(0, 0);
  float dYc = pc(1, 0) - p2(1, 0);
  float dX21 = p2(0, 0) - p1(0, 0);
  float dY12 = p1(1, 0) - p2(1, 0);
  float D = dY12 * (p0(0, 0) - p2(0, 0)) + dX21 * (p0(1, 0) - p2(1, 0));
  float sa = dY12 * dXa + dX21 * dYa;
  float sb = dY12 * dXb + dX21 * dYb;
  float sc = dY12 * dXc + dX21 * dYc;
  float ta = (p2(1, 0) - p0(1, 0)) * dXa + (p0(0, 0) - p2(0, 0)) * dYa;
  float tb = (p2(1, 0) - p0(1, 0)) * dXb + (p0(0, 0) - p2(0, 0)) * dYb;
  float tc = (p2(1, 0) - p0(1, 0)) * dXc + (p0(0, 0) - p2(0, 0)) * dYc;
  if (D < 0) return ((sa >= 0 && sb >= 0 && sc >= 0) ||
                     (ta >= 0 && tb >= 0 && tc >= 0) ||
                     (sa+ta <= D && sb+tb <= D && sc+tc <= D));
  return ((sa <= 0 && sb <= 0 && sc <= 0) ||
          (ta <= 0 && tb <= 0 && tc <= 0) ||
          (sa+ta >= D && sb+tb >= D && sc+tc >= D));
}

void 
Processor::dispPair(int c1, int c2)
{
  cv::Mat im1, im2;
  im1 = cv::imread(loader.imagePaths[c1].string(), -1);
  im2 = cv::imread(loader.imagePaths[c2].string(), -1);

  cv::Mat dst(cv::Size(im1.cols*2,im1.rows),im1.type(),cv::Scalar::all(0));
  cv::Mat matRoi = dst(cv::Rect(0,0,im1.cols,im1.rows));
  im1.copyTo(matRoi);
  matRoi = dst(cv::Rect(im2.cols,0,im2.cols,im2.rows));
  im2.copyTo(matRoi);

  cv::imshow("Pair", dst);
}

// Returns 2 if unusable, 1 if pos pair, 0 if neg pair
float Processor::cloudOverlap(int c1, int c2) {
  /*
  float max_z = 3;
  // Find geometric overlap using calibration matrix and position
  Calibration cal1 = loader.calibs[c1];
  Calibration cal2 = loader.calibs[c2];
  Eigen::Vector4f end_l1 (-cal1.cx*max_z/cal1.fx, 0, max_z, 1);
  Eigen::Vector4f end_r1 (cal1.cx*max_z/cal1.fx, 0, max_z, 1);
  Eigen::Vector4f origin1(0, 0, 0, 1);

  end_l1 = loader.transforms[c1] * end_l1;
  end_r1 = loader.transforms[c1] * end_r1;
  origin1 = loader.transforms[c1] * origin1;

  Eigen::Vector4f end_l2 (-cal2.cx*max_z/cal2.fx, 0, max_z, 1);
  Eigen::Vector4f end_r2 (cal2.cx*max_z/cal2.fx, 0, max_z, 1);
  Eigen::Vector4f origin2(0, 0, 0, 1);

  end_l2 = loader.transforms[c2] * end_l2;
  end_r2 = loader.transforms[c2] * end_r2;
  origin2 = loader.transforms[c2] * origin2;

  if ((cross2(end_l1, end_r1, origin1, 
              end_l2, end_r2, origin2) and
       cross2(end_l2, end_r2, origin2, 
              end_l1, end_r1, origin1))) {
    return 0;
  }

  // Compute hull overlap
  pcl::CropHull<pcl::PointXYZRGB> cropper;
  vector<int> indices1;
  cropper.setInputCloud(hulls[c2]);
  cropper.setHullCloud(hulls[c1]);
  cropper.setHullIndices(verts[c1]);
  cropper.filter(indices1);
  float hv1 = float(indices1.size()) / hulls[c1]->size();

  vector<int> indices2;
  cropper.setInputCloud(hulls[c1]);
  cropper.setHullCloud(hulls[c2]);
  cropper.setHullIndices(verts[c2]);
  cropper.filter(indices2);
  float hv2 = float(indices2.size()) / hulls[c2]->size();

  float hull_overlap = min(hv1, hv2);

  if (hull_overlap == 0) {
    //cout << "NEG" << endl;
    return 0;
  }

  // Compute rotational difference
  Eigen::AngleAxisf rot1;
  rot1.fromRotationMatrix(loader.transforms[c1].block<3,3>(0,0));
  Eigen::AngleAxisf rot2;
  rot2.fromRotationMatrix(loader.transforms[c2].block<3,3>(0,0));
  float diff = (rot1.angle() - rot2.angle());

  if (diff > maxAngle) { // Rotational angle is not enough to disqualify pair
    return -1; // magic value
  }
  */

  // Back project pc 1 to 2
  cv::Mat dep1 = depthImages[c1];
  cv::Mat dep2 = depthImages[c2];
  double max_dist1, min_dist1, max_dist2, min_dist2;
  minMaxLoc(dep1, &min_dist1, &max_dist1);
  minMaxLoc(dep2, &min_dist2, &max_dist2);

  float dist = (loader.transforms[c1].inverse()*loader.transforms[c2]).block<3, 1>(0, 3).norm();
  // std::cout << "DS" << dist << std::endl;
  // std::cout << "MS" << (max_dist1 + max_dist2)/loader.ds->getDepthFactor() << std::endl;
  if (dist > (max_dist1 + max_dist2)/loader.ds->getDepthFactor()) {
    return 0;
  }

  BoundingBox full(0, 0, dep1.cols, dep1.rows);

  cv::Mat backproj_bbx;

  //float overlap = back_project(
      //dep1, dep2,
      //loader.transforms[c1], loader.transforms[c2],
      //loader.calibs.at(c1), loader.calibs.at(c2),
      //full, full,
      //loader.ds->getDepthFactor(), backproj_bbx);

  float overlap = back_project(
      dep1, dep2, clouds[c2],
      loader.transforms[c1], loader.transforms[c2],
      loader.calibs.at(c1), loader.calibs.at(c2),
      full, full,
      loader.ds->getDepthFactor(), backproj_bbx);

  // Debug
  // if (overlap > minAvgProjectedValues) {
  //   cv::imshow("Query Dep", dep1/1024);
  //   cv::imshow("Cand Dep", dep2/1024);
  //   cout << loader.depthPaths[c1].string() << endl;
  //   cout << loader.depthPaths[c2].string() << endl;
  //   cv::Mat backproj_disp;
  //   backproj_bbx.convertTo(backproj_disp, CV_8UC1, 255);
  //   cv::imshow("Backproj", backproj_disp);
  //   std::cout << overlap << std::endl;
  //   cv::waitKey(0);
  // }
  return overlap;
  /*
  Cloud::Ptr transformed (new Cloud);
  Eigen::Matrix4f t = loader.transforms[c1].inverse();
  pcl::transformPointCloud(*clouds.at(c2), *transformed, t);

  cv::Mat bp1, bp2;
  cv::Mat depth = cv::imread(loader.depthPaths[c1].string(), -1);
  colorCloudToMat(bp1, transformed, loader.calibs.at(c1), depth.cols, depth.rows, loader.ds->getDepthFactor());
  cloudToMat<pcl::PointXYZRGB>(bp1, transformed, loader.calibs.at(c1), depth.cols, depth.rows, loader.ds->getDepthFactor());

  depthToCloud<pcl::PointXYZRGB>(const cv::Mat & depth,
             const Calibration calib,
             const float factor,
             const BoundingBox & bb,
             boost::shared_ptr< pcl::PointCloud<T> > out)
  //colorCloudToMat(bp2, clouds.at(c1), loader.calibs.at(c1), 1000, 1000, 512);
  //cout << bp << flush << endl;
  int iters = 3;
  for (int i = 0; i < iters; i++) {
    cv::pyrDown(bp1, bp1);
    //cv::pyrDown(bp2, bp2);
  }
  cv::threshold(bp1, bp1, 0.001, 1, 0);
  //cv::threshold(bp2, bp2, 0.001, 1, 0);

  double avg = cv::mean(bp1)[0];
  return avg;
  */
  //cv::Mat disp;
  //cv::normalize(bp1, disp, 255, 0, cv::NORM_MINMAX, CV_8UC1);
  //cv::imshow("BP", disp);
  //cout << "Display pair" << endl;
  //dispPair(c1, c2);
  //cv::waitKey(0);
  //if (avg < minAvgProjectedValues) {
    //return avg/minAvgProjectedValues * 0.3; // magic value
  //}

  //if (minAvgProjectedValues/2  < avg && avg < minAvgProjectedValues) {
    //cout << avg << endl;
    //dispPair(c1, c2);
    //cv::waitKey(0);
    //return 0;
  //} else if (avg < minAvgProjectedValues) {
    //return 0;
  //}
  //double mul = 1000;
  //cout << log(avg*mul)/log(mul)<< endl;
  //return log(avg*mul)/log(mul); // magic value

  /*
  pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
  icp.setMaxCorrespondenceDistance (2.05);
  icp.setInputSource(clouds.at(c1));
  icp.setInputTarget(clouds.at(c2));

  pcl::PointCloud<pcl::PointXYZRGB> Final;
  icp.align(Final);
  if (icp.getFitnessScore() < minICPConfidence) {
    cout << "ICP" << endl;
    return 2;
  }

  */
  //return 1;
}

void Processor::compare(cv::Mat & dst)
{
  cout << loader.imagePaths.size() << endl;
  // dst = cv::Mat(cv::Size(loader.imagePaths.size(), loader.imagePaths.size()), CV_8UC1, cv::Scalar(2));
  dst = cv::Mat(cv::Size(clouds.size(), clouds.size()), CV_32FC1, cv::Scalar(0));
#pragma omp parallel for
  for(uint i=0; i<min((uint)maxClouds, (uint)loader.imagePaths.size()); i++)
  {
    //if (clouds[i].points_.size() == 0)
      //continue;
    dst.at<float>(i, i) = 1;
    for(uint j=0; j<i; j++)
    {
      if (i==j)
        continue;
      //if (clouds[j].points_.size() == 0)
        //continue;
      float over = cloudOverlap(i, j);
      dst.at<float>(i, j) = over;
      dst.at<float>(j, i) = over;

      // if (over < eps) {
      //   dst.at<char>(i, j) = 0;
      //   dst.at<char>(j, i) = 0;
      // } else if (over > minAvgProjectedValues) {
      //   dst.at<char>(i, j) = 1;
      //   dst.at<char>(j, i) = 1;
      // } else {
      //   dst.at<char>(i, j) = 2;
      //   dst.at<char>(j, i) = 2;
      // }
    }
  }
}

void Processor::save_paths(fs::path path) {
  fs::path dir = path.parent_path();
  if(!(fs::exists(dir))){
    if (fs::create_directory(dir)) {
        cout << "Successfully created directory at " << dir << endl;
    }
  }
  cout << "Saving image paths to " << path << endl;
  ofstream file;
  file.open(path.string().c_str(), ios::out | ios::trunc);

  vector<fs::path> imgs = loader.ds->loadCleanImagePaths();
  for (size_t i=0; i<imgs.size()-skipClouds; i+=(skipClouds+1))
  {
    file << imgs[i].string() << endl;
    file.flush();
  }
  file.close();
}
