#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <set>

#include "util.h"
#include "Stanford.h"
#include "json.hpp"
#include <assert.h>
#include "Dataset.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/filesystem.hpp>

using json = nlohmann::json;
namespace fs = boost::filesystem;

using namespace std;

Stanford::Stanford(const fs::path & basePath, int subset) {
  this->subset = subset;
  dataPath = basePath / STANFORD_DATASETS[subset] / "data";
  locationNames = makeList(dataPath / "rgb");
}

string Stanford::getName() {
  return STANFORD_DATASETS[subset];
}

float
Stanford::getDepthFactor() {
  return 512;
}

float Stanford::depthFactor = 512;

vector<string>
Stanford::makeList(const fs::path & modalityPath)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot access path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  vector<string> v;
  regex fname_rgx ("(camera_.*_.*_.*_frame_.*_domain_)");
  while ((dp = readdir(dirp)) != NULL) {
    smatch matches;
    string temp_to_temp = string(dp->d_name);
    if(regex_search(temp_to_temp, matches, fname_rgx)) {
      v.push_back(matches[0].str());
    }
  }
  closedir(dirp);
  sort(v.begin(), v.end());

  return v;
}

vector<string>
Stanford::makeListOfRooms(const fs::path & modalityPath)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot access path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  set<string> v;
  regex fname_rgx ("camera_.*_(.*)_.*_frame_.*_domain_");
  while ((dp = readdir(dirp)) != NULL) {
    smatch matches;
    string temp_to_temp = string(dp->d_name);
    if(regex_search(temp_to_temp, matches, fname_rgx)) {
      v.emplace(matches[1].str());
    }
  }
  closedir(dirp);
  vector<string> out;
  std::copy(v.begin(), v.end(), std::back_inserter(out));
  sort(out.begin(), out.end());

  return out;
}

vector<string>
Stanford::makeRoomList(const fs::path & modalityPath, const string & room)
{
  struct stat info;

  if( stat( modalityPath.string().c_str(), &info ) != 0 ) {
    printf( "Cannot access path: %s\n", modalityPath.string().c_str() );
    exit(-1);
  }

  DIR* dirp = opendir(modalityPath.string().c_str());
  struct dirent * dp;
  vector<string> v;
  regex fname_rgx ("(camera_.*_"+ room +"_.*_frame_.*_domain_)");
  while ((dp = readdir(dirp)) != NULL) {
    smatch matches;
    string temp_to_temp = string(dp->d_name);
    if(regex_search(temp_to_temp, matches, fname_rgx)) {
      v.push_back(matches[0].str());
    }
  }
  closedir(dirp);
  sort(v.begin(), v.end());

  return v;
}

string
Stanford::getRoomName(
    const fs::path & base_path,
    const int dataset_num,
    const int room_num)
{
  fs::path dataPath = base_path / STANFORD_DATASETS[dataset_num] / "data";
  vector<string> list = Stanford::makeListOfRooms(dataPath / "rgb");
  return list[room_num];
}

Eigen::Matrix4f
Stanford::loadGroundTruth(const fs::path & poseFilePath) {
  std::ifstream t(poseFilePath.string());
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());
  json js = json::parse(str);

  // Camera center
  Eigen::Matrix4f translation = Eigen::Matrix4f::Zero();
  translation(0, 3) = js["camera_location"][0];
  translation(1, 3) = js["camera_location"][1];
  translation(2, 3) = js["camera_location"][2];

  // Orientation in Panorama
  Eigen::Matrix4f RT;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      RT(i,j) = js["camera_rt_matrix"][i][j];
    }
    RT(i, 3) = 0;
  }
  for (int j=0; j<4; j++) {
    RT(3, 0) = 0;
  }
  RT(3,3) = 1;

  Eigen::Affine3f RT_transform (RT);

  // Compose orientation in rig
  Eigen::Quaternionf q2;
  float roll = js["rotation_from_original_to_point"][2];
  float pitch = js["rotation_from_original_to_point"][1];
  float yaw = js["rotation_from_original_to_point"][0];
  q2 = Eigen::AngleAxisf(roll, Eigen::Vector3f::UnitZ())
    * Eigen::AngleAxisf(pitch, Eigen::Vector3f::UnitY())
    * Eigen::AngleAxisf(yaw, Eigen::Vector3f::UnitX());
  Eigen::Affine3f rotation2( q2 );

  Eigen::Quaternionf q;
  roll = js["camera_original_rotation"][2];
  pitch = js["camera_original_rotation"][1];
  yaw = js["camera_original_rotation"][0];
  q = Eigen::AngleAxisf(roll, Eigen::Vector3f::UnitZ())
    * Eigen::AngleAxisf(pitch, Eigen::Vector3f::UnitY())
    * Eigen::AngleAxisf(yaw, Eigen::Vector3f::UnitX());

  Eigen::Affine3f rotation( q );
  // Orientation in rig
  Eigen::Matrix4f mat = rotation2.rotate(q).matrix();

  // Create final transformation
  return (mat*RT*mat) + translation;
}

vector<Eigen::Matrix4f> Stanford::loadGroundTruths() {
  vector<Eigen::Matrix4f> transforms;
  transforms.reserve(locationNames.size());
  for (size_t i=0; i < locationNames.size(); i++) {
    const fs::path posePath = dataPath / "pose" / fs::path(locationNames[i] + "pose.json");
    transforms.push_back(Stanford::loadGroundTruth(posePath));
  }
  return transforms;
}

vector<fs::path> Stanford::loadCleanImagePaths() {
  vector<fs::path> paths;
  paths.reserve(locationNames.size());
  for (size_t i=0; i < locationNames.size(); i++) {
    const fs::path rgbPath = fs::path(getName()) / "data/rgb" / fs::path(locationNames.at(i) + "rgb.png");
    paths.push_back(rgbPath);
  }
  return paths;
}

vector<fs::path> Stanford::loadImagePaths() {
  vector<fs::path> paths;
  paths.reserve(locationNames.size());
  for (size_t i=0; i < locationNames.size(); i++) {
    const fs::path rgbPath = dataPath / "rgb" / fs::path(locationNames.at(i) + "rgb.png");
    paths.push_back(rgbPath);
  }
  return paths;
}

vector<fs::path> Stanford::loadDepthPaths() {
  vector<fs::path> paths;
  paths.reserve(locationNames.size());
  for (size_t i=0; i < locationNames.size(); i++) {
    const fs::path depthPath = dataPath / "depth" / fs::path(locationNames.at(i) + "depth.png");
    paths.push_back(depthPath);
  }
  return paths;
}

vector<Calibration> Stanford::loadCalibrations() {
  vector<Calibration> cs;
  cs.reserve(locationNames.size());
  for (size_t i=0; i < locationNames.size(); i++) {
    const fs::path posePath = dataPath / "pose" / fs::path(locationNames[i] + "pose.json");
    // Get fx fy
    std::ifstream t(posePath.string());
    if (!t.good()) {
      cout << "Failed to open calib file: " << posePath << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json j = json::parse(str);
    Calibration c (j["camera_k_matrix"][0][0],
                   j["camera_k_matrix"][1][1],
                   540, 540);
    cs.push_back(c);
  }
  return cs;
}

/*
 * File formats
 *
 * pose/camera_<id>_<location>_<location num>_frame_<fnum>_domain_pose.json
 * {
 *   "camera_k_matrix": [[843.8359375, 0.0, 540.0],
 *                       [0.0, 843.8359375, 540.0],
 *                       [0.0, 0.0, 1.0]],
 *   "field_of_view_rads": 1.1385339084500217,
 *   "camera_rt_matrix": [
 *     [-0.2596034109592438, 0.9657081961631775, 0.00370126124471426, 6.683495044708252],
 *     [0.1962047517299652, 0.05649605765938759, -0.9789340496063232, -2.8775875568389893],
 *     [-0.9455738067626953, -0.25340840220451355, -0.20414310693740845, 20.74506378173828]],
 *   "rotation_from_original_to_point": 
 *     [-0.14743150770664215, 0.15250426530838013, 2.652224540710449],
 *   "point_uuid": "870532d7e14240c2ad412ff708be7112",
 *   "camera_original_rotation": [1.57687246799469, 0.0010217042872682214, -0.8303837180137634],
 *   "camera_location": [21.915642, -1.03476, 1.393256],
 *   "frame_num": 0,
 *   "final_camera_rotation": 
 *     [1.3652065992355347, -0.0037012696266174316, 1.833409667015    0757],
 *   "camera_uuid": "870532d7e14240c2ad412ff708be7112",
 *   "room": "office_4_3"
 * }
 * rgb/camera_<id>_<location>_<location num>_frame_<fnum>_domain_rgb.json
 * depth/camera_<id>_<location>_<location num>_frame_<fnum>_domain_depth.json
 */

