FROM ubuntu:bionic
LABEL maintainer "Alexander Mai alexandertmai@gmail.com"

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/
ENV DEBIAN_FRONTEND=noninteractive

# Python takes longer with install
RUN apt-get update && apt-get install -y --no-install-recommends \
        # misc
        python3 python3-pip wget apt-utils unzip build-essential sudo ca-certificates net-tools vim git cmake \
        # opencv
        python3-dev python3-numpy python-dev python-numpy libopencv-dev \
        # open3d
        libglfw3-dev libglew-dev libeigen3-dev openmpi-common openmpi-bin \
        && echo "imap fd <Esc>" > ~/.vimrc

# Get torchlib
WORKDIR /root/
RUN pip3 install setuptools wheel && \
    # Clone opencv, Open3D, and Apex
    git clone --recursive https://github.com/intel-isl/Open3D && mkdir /root/Open3D/build

WORKDIR /root/
RUN git clone https://github.com/fmtlib/fmt \
    && mkdir fmt/build \
    && cd fmt/build \
    && cmake .. \
    && make install

# Install open3d
WORKDIR /root/Open3D/build
RUN ../util/scripts/install-deps-ubuntu.sh assume-yes
RUN cmake -DBUILD_PYBIND11=OFF -DCMAKE_INSTALL_PREFIX=/usr/local .. && make -j16 && make install

# Clean up
WORKDIR /root/
RUN rm -r Open3D

RUN apt-get install -y --no-install-recommends libboost-filesystem-dev libboost-system-dev libboost-program-options-dev libzmqpp-dev libcurl4-gnutls-dev

# COMPILE OVERLAP_LIB
ADD . /root/overlap
WORKDIR /root/overlap/build
RUN rm -r * && cmake .. && make -j4

WORKDIR /root/

